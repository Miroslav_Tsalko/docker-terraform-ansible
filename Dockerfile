FROM alpine
LABEL MAINTAINER M_Tsalko
# Defining Environment 
ENV TerraformVer="0.14.3"
# This variables should be defined if you storing terraform STATE lockaly (without remote backend).
#ENV AWS_ACCESS_KEY_ID="YOUR_AWS_ID" 
#ENV AWS_SECRET_ACCESS_KEY="YOUR_AWS_Key"

# Installing packages
RUN apk update && apk upgrade && apk add \
    python3 \
    libffi-dev \
    wget \
    unzip \
    openssh-client \
    mc \
    ansible
# Installing Terraform
RUN wget -O terraform.zip --quiet "https://releases.hashicorp.com/terraform/$TerraformVer/terraform_"$TerraformVer"_linux_amd64.zip" \
  && unzip terraform.zip \
  && mv terraform /usr/bin \
  && rm terraform.zip
